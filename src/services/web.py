import logging
from http import HTTPStatus

from flask import Flask, jsonify, request, make_response
import psycopg2
import psycopg2.extras

from db import get_db_connection
import tasks
import config

app = Flask(__name__)
logger = logging.getLogger(__name__)


@app.route("/")
def index():
    return jsonify(success=True)


@app.route("/photos/pending", methods=["GET"])
def get_pending_photos():
    connection = get_db_connection()
    cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
    get_pending_photos_query = "select * from photos where status = 'pending'"
    try:
        cursor.execute(get_pending_photos_query)
    except psycopg2.Error as e:
        logger.error("Error when executing query: {get_pp_query}".format(get_pp_query=get_pending_photos_query))
        connection.close()
        return make_response(jsonify(message="Database connection error", error=str(e)),
                                     HTTPStatus.INTERNAL_SERVER_ERROR)
    pending_photos = cursor.fetchall()
    result = []
    for row in pending_photos:
        result.append(dict(row))
    connection.close()
    return jsonify(result)


@app.route("/photos/process", methods=["POST"])
def process_photos():
    photo_uuids = request.get_json()
    for photo_uuid in photo_uuids:
        tasks.process_photo({"photo_uuid": photo_uuid})

    return jsonify(success=True)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    app.run(host='0.0.0.0', port=3000)
