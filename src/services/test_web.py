from web import app, index, tasks
import web
import json
from flask_testing import TestCase
from unittest.mock import patch, Mock


class WebTest(TestCase):

    def create_app(self):
        return app

    def test_index(self):
        response = self.client.get('/')
        assert response.json['success'] is True

    @patch.object(tasks, 'process_photo')
    def test_process_photos(self, mock_process_photo):
        uuids = [
            '123'
        ]
        response = self.client.post('/photos/process', json=json.dumps(uuids))
        assert response.json['success'] is True

    @patch.object(web, 'get_db_connection')
    def test_get_pending_photos(self, mock_db_connection):
        expected = [
            {
                "uuid": "123"
            },
            {
                "uuid": "456"
            }
        ]
        mock_con = mock_db_connection.return_value  # result of psycopg2.connect(**connection_stuff)
        mock_cur = mock_con.cursor.return_value  # result of con.cursor(cursor_factory=DictCursor)
        mock_cur.fetchall.return_value = expected  # return this when calling cur.fetchall()

        response = self.client.get('/photos/pending')
        assert response.json[0]['uuid'] == "123"
        assert response.json[1]['uuid'] == "456"
