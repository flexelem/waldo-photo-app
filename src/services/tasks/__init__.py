import logging
from io import BytesIO
from PIL import Image

from kuyruk import Kuyruk
from kuyruk.config import Config
import psycopg2
import psycopg2.extras
import requests

import tasks
from db import get_db_connection

config = Config()
config.RABBIT_PASSWORD = "1234"
config.RABBIT_USER = "rabbitmq"
config.RABBIT_HOST = "rabbitmq"
config.RABBIT_PORT = "5672"

kuyruk = Kuyruk(config)
logger = logging.getLogger(__name__)


@kuyruk.task(queue='photo-processor')
def process_photo(message):
    photo_uuid = message['photo_uuid']
    connection = get_db_connection()
    cursor = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)
    update_photos_query = "update photos set status = 'processing' where uuid='{uuid}'".format(uuid=photo_uuid)

    cursor.execute(update_photos_query)
    get_photo_query = "select * from photos where uuid = '{uuid}'".format(uuid=photo_uuid)
    cursor.execute(get_photo_query)
    connection.commit()

    processed_photo_row = cursor.fetchone()
    logger.info("Downloading image from: {url}".format(url=processed_photo_row['url']))
    response = requests.get(processed_photo_row['url'])
    logger.info("Download completed from: {url}".format(url=processed_photo_row['url']))
    thumbnail_created_success = generate_thumbnail(response.content, photo_uuid)

    if thumbnail_created_success:
        insert_photo_thumbnail_query = "insert into photo_thumbnails " \
            "(photo_uuid, width, height, url) values ('{photo_uuid}', {width}, {height}, '{url}')".format(
                photo_uuid=photo_uuid,
                height=320,
                width=320,
                url=photo_uuid + '.jpg'
            )
        cursor.execute(insert_photo_thumbnail_query)
        update_photos_query = "update photos set status = 'completed' where uuid='{uuid}'".format(uuid=photo_uuid)
        cursor.execute(update_photos_query)
        connection.commit()
    else:
        update_photos_query = "update photos set status = 'failed' where uuid='{uuid}'".format(uuid=photo_uuid)
        cursor.execute(update_photos_query)
        connection.commit()

    connection.close()


def generate_thumbnail(bytes_arr, file_name):
    try:
        im = Image.open(BytesIO(bytes_arr))
        size = 320, 320
        im.thumbnail(size)
        path_to_save = "/waldo-app-thumbs/{file_name}".format(file_name=file_name)
        im.save(path_to_save + '.jpg')
    except Exception as exception:
        logger.error("Generating thumbnail for photo uuid:{uuid} failed with reason:{exception}".format(uuid=file_name, exception=exception))
        return False

    return True
