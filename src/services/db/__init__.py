import psycopg2
import psycopg2.extras

import config


def get_db_connection():
    connection = psycopg2.connect(user=config.POSTGRES_USER,
                                  password=config.POSTGRES_PASSWORD,
                                  host=config.POSTGRES_HOST,
                                  port=config.POSTGRES_PORT,
                                  database=config.POSTGRES_DB)
    return connection
