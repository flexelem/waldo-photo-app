#! /usr/bin/env sh

./scripts/wait-for.sh postgres:5432 -- echo "postgres is up"
./scripts/wait-for.sh rabbitmq:5672 -- echo "rabbitmq is up"

echo "Running Tests"
find -name '*.pyc' -delete
find -name __pycache__ -delete
python -m pytest /app

test_result=$?

echo $test_result
if [ $test_result != 0 ]; then
    echo "TESTS FAILED"
    exit 1
fi

echo "Starting Web Server"
# python src/services/web.py
nohup /bin/sh -c "python /app/src/services/web.py &"

echo "Starting Worker"
cd src/services
kuyruk --app tasks.kuyruk worker --queue photo-processor

